<h1> vue-admin（ant-design-vue） </h1>

## 特性

- 支持 PC、手机端、平板；
- 提供超过 50 余项全局精细化配置；
- 支持后端渲染动态路由
- 拥有完整的登录鉴权和前后端多种配置的动态路由流程
- 支持前端控制路由权限 intelligence、后端控制路由权限 all 模式
- 支持 mock 自动生成自动导出功能
- 支持 scss 自动排序，eslint 自动修复
- 支持登录 RSA 加密
- 支持打包自动生成 7Z 压缩包以及自动化部署
- 支持 errorlog 错误拦截
- 支持多主题、多布局切换

## git提交规范

- ✨feat: 新功能
- 🐛fix: 修复bug
- 📂docs: 文档改变
- 🌈style: 代码格式改变
- 🔨refactor: 某个已有功能重构
- ⚡️perf: 性能优化
- ❗️test: 增加测试
- 📦build: 改变了build工具 如 grunt换成了 npm
- ↪️revert: 撤销上一次的 commit
- 🧩chore: 构建过程或辅助工具的变动
