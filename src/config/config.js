/**
 * @description 导出自定义配置
 **/
const config = {
  layout: 'vertical',
  title: '运维自动化平台',
  donation: false,
  templateFolder: 'project',
}
module.exports = config
