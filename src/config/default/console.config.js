module.exports = {
  webpackBarName: 'vue-admin',
  webpackBanner: ' build: vue-admin \n vue-admin author: PaoYou © ',
  donationConsole() {
    const chalk = require('chalk')
    console.log(chalk.green(`> 如果您不希望显示以上信息，可在config中配置关闭`))
    console.log('\n')
  },
}
