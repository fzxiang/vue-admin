/**
 * @description 导出默认配置(通用配置|主题配置|网络配置)
 **/
const setting = require('./setting.config')
const theme = require('./theme.config')
const network = require('./net.config')
const consoleConfig = require('./console.config')

module.exports = { setting, theme, network, consoleConfig }
