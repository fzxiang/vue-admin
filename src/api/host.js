import request from '@/utils/request'

const api = {
  nodeSync: '/host/node/sync',
  node: '/host/node',
  assetSync: '/host/asset/sync',
  detail: '/host/asset',
}

// 主机管理 - 同步节点
export function syncNodeApi() {
  return request({
    url: api.nodeSync,
    method: 'post',
  })
}

// 主机管理-节点列表
export function getNodeApi(id, parameter) {
  return request({
    url: api.node + (id ? `/${id}` : ''),
    method: 'get',
    params: parameter,
  })
}

// 主机管理 - 同步主机
export function syncHostApi() {
  return request({
    url: api.assetSync,
    method: 'post',
  })
}

// 主机管理 - 主机列表
export function getHostApi(node, parameter) {
  parameter.mark = null
  parameter.old = undefined
  return request({
    url: `${api.node}/${node}/asset`,
    method: 'get',
    params: parameter,
  })
}

// 主机管理 - 主机详情
export function getDetailHost(id) {
  return request({
    url: `${api.detail}/${id}`,
    method: 'get',
  })
}
